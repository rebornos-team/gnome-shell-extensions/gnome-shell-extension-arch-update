# gnome-shell-extension-arch-update

Convenient indicator for Arch Linux updates in GNOME Shell.

https://github.com/RaphaelRochet/arch-update

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/gnome-shell-extension-arch-update.git
```

